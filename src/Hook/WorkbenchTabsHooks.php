<?php

namespace Drupal\workbench_tabs\Hook;

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\workbench_tabs\WorkbenchTabsInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Hook implementations for Workbench Tabs.
 */
class WorkbenchTabsHooks {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The Workbench Tabs info service.
   *
   * @var \Drupal\workbench_tabs\WorkbenchTabsInfo
   */
  protected $workbenchTabsInfo;

  /**
   * Constructs a new WorkbenchTabsHooks object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\workbench_tabs\WorkbenchTabsInfo $workbench_tabs_info
   *   The Workbench tabs info service.
   */
  public function __construct(AccountProxyInterface $current_user, WorkbenchTabsInfo $workbench_tabs_info) {
    $this->currentUser = $current_user;
    $this->workbenchTabsInfo = $workbench_tabs_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('workbench_tabs.info'),
    );
  }

  /**
   * Provide help for the Workbench Tabs settings page.
   */
  #[Hook('help')]
  public function help(string $route_name, RouteMatchInterface $route_match):?string {
    switch ($route_name) {
      case 'workbench_tabs.settings':
        // Generate link for module's permission, if the user has access to it.
        $perms_text = '';
        if ($this->currentUser->hasPermission('administer permissions')) {
          $perms_link = Link::createFromRoute('permissions', 'user.admin_permissions', [], ['fragment' => 'module-workbench_tabs'])->toString();
          $perms_text = $this->t('Use @permissions to control which user roles see Workbench Tabs.', ['@permissions' => $perms_link]);
        }

        return '<p>' . $this->t("Workbench Tabs moves the local task tabs and Drupal messages into the Toolbar.") . ' ' . $perms_text . '</p>';
    }

    return NULL;
  }

  /**
   * Provide Workbench Tabs theming components.
   */
  #[Hook('theme')]
  public function theme(): array {
    return [
      'workbench_tabs' => [
        'render element' => 'element',
      ],
      'workbench_tabs_menu_local_task' => [
        'render element' => 'element',
      ],
      'workbench_tabs_menu_local_tasks' => [
        'variables' => ['primary' => NULL, 'secondary' => NULL],
      ],
      'workbench_tabs_status_messages' => [
        'variables' => ['status_headings' => [], 'message_list' => NULL],
      ],
    ];
  }

  /**
   * Add Workbench Tabs functionality to the 'page_top' region.
   */
  #[Hook('page_top')]
  public function pageTop(array &$page_top): void {
    if ($this->workbenchTabsInfo->applyWorkbenchTabs()) {
      $account = $this->currentUser;

      $page_top['workbench_tabs'] = [
        '#theme' => 'workbench_tabs',
        '#access' => $account->hasPermission('use workbench_tabs'),
      ];

      $page_top['workbench_tabs']['messages'] = [
        '#type' => 'workbench_tabs_status_messages',
        '#cache' => [
          'contexts' => [
            'url.path',
            'user.roles',
          ],
        ],
      ];

      $page_top['workbench_tabs']['tabs'] = [
        '#type' => 'workbench_tabs_local_tasks',
      ];
    }
  }

  /**
   * Hide the local tasks block when Workbench Tabs is enabled on a page.
   */
  #[Hook('block_build_alter')]
  public function blockBuildAlter(array &$build, BlockPluginInterface $block): void {
    if ($this->workbenchTabsInfo->applyWorkbenchTabs() && $block->getPluginId() == 'local_tasks_block') {
      $build['#access'] = FALSE;
    }
  }

}
