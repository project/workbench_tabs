<?php

namespace Drupal\workbench_tabs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for Workbench Tabs.
 *
 * @package Drupal\workbench_tabs\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'workbench_tabs_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['workbench_tabs.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('workbench_tabs.settings');

    $form['enable_for_admin_theme'] = [
      '#title' => 'Enable Workbench Tabs:',
      '#type' => 'radios',
      '#options' => [
        0 => 'Only on non-admin themes',
        1 => 'Everywhere',
      ],
      '#default_value' => (int) $config->get('enable_for_admin_theme'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('workbench_tabs.settings')
      ->set('enable_for_admin_theme', $values['enable_for_admin_theme'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
