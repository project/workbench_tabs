<?php

namespace Drupal\workbench_tabs;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

/**
 * Service class to determine when Workbench Tabs should be applied to a page.
 *
 * @package Drupal\workbench_tabs
 */
class WorkbenchTabsInfo {

  /**
   * The Drupal config.factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Drupal theme.negotiator service.
   *
   * @var \Drupal\Core\Theme\ThemeNegotiatorInterface
   */
  protected $adminThemeNegotiator;

  /**
   * The Drupal route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * Constructor with dependency injection.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Drupal's config.factory service.
   * @param \Drupal\Core\Theme\ThemeNegotiatorInterface $adminThemeNegotiator
   *   Drupal's theme.negotiator service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $currentRouteMatch
   *   Drupal's current_route_match service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, ThemeNegotiatorInterface $adminThemeNegotiator, RouteMatchInterface $currentRouteMatch) {
    $this->configFactory = $configFactory;
    $this->adminThemeNegotiator = $adminThemeNegotiator;
    $this->currentRouteMatch = $currentRouteMatch;
  }

  /**
   * Whether to apply Workbench Tabs on the current page.
   *
   * @return bool
   *   TRUE if Workbench Tabs should take over the local tasks and Drupal
   *   message display on this page.
   */
  public function applyWorkbenchTabs() {
    // Enabling for the admin theme means it's always applied.
    if ($this->configFactory->get('workbench_tabs.settings')->get('enable_for_admin_theme')) {
      return TRUE;
    }

    // Otherwise, it's only applied on non-admin themes.
    if (!$this->adminThemeNegotiator->applies($this->currentRouteMatch)) {
      return TRUE;
    }

    return FALSE;
  }

}
