/**
 * @file
 * Collapse the workbench_tabs messages.
 */

((Drupal) => {
  // Close the Drupal messages display.
  const closeMessages = (messageContents, messageTrigger) => {
    messageTrigger.classList.add('is-closed');
    messageContents.classList.add('is-closed');
  };

  // Open the Drupal messages display.
  const openMessages = (messageContents, messageTrigger) => {
    messageTrigger.classList.remove('is-closed');
    messageContents.classList.remove('is-closed');
  };

  // Toggle the Drupal messages display.
  const toggleMessages = (messageContents, messageTrigger) => {
    if (messageContents.matches('.is-closed')) {
      openMessages(messageContents, messageTrigger);
    } else {
      closeMessages(messageContents, messageTrigger);
    }
  };

  // Generate a function to handle the window scroll events for this set of messages.
  const scrollFactory = (messageHeight, messageContents, messageTrigger) => {
    return () => {
      if (messageHeight > 0 && window.pageYOffset > messageHeight) {
        // Reevaluate message height because user interactions can change it,
        // but we don't want to calculate this on every scroll event.
        messageHeight = messageContents.clientHeight;

        if (window.pageYOffset > messageHeight) {
          closeMessages(messageContents, messageTrigger);

          // Prevent the messages from being collapsed again; if they're open after this,
          // it is because the user opened them intentionally.
          messageHeight = messageContents.clientHeight;
        }
      }
    };
  };

  Drupal.behaviors.workbenchTabs = {};
  Drupal.behaviors.workbenchTabs.attach = (context) => {
    const messageTrigger = context.querySelector('.workbench-tabs__trigger');
    const messageContents = context.querySelector('.workbench-tabs__message');

    if (!messageTrigger) {
      return;
    }

    // Expand and collapse the message display in the toolbar.
    messageTrigger.onclick = (e) => {
      e.preventDefault();
      toggleMessages(messageContents, messageTrigger);
    };

    // Close the drawer when we scroll past it.
    window.addEventListener(
      'scroll',
      scrollFactory(
        messageContents.clientHeight,
        messageContents,
        messageTrigger,
      ),
      { passive: true },
    );
  };
})(Drupal);
