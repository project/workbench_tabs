# Workbench Tabs

Workbench Tabs moves the local task tabs and messages from the theme into the Toolbar, so that custom themes don't need to provide these administrative elements.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/workbench_tabs

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/workbench_tabs
